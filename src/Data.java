import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Data {
    private int id;
    private String name;
    private int age;
    private String address;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void insertUser(){
        String query = String.format("INSERT INTO user(name,age,address) VALUES(%s, %s, %s)", "'"+name+"'", age, "'"+address+"'");
        try {
            DBConnection.setData(query);
        } catch (Exception e) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public int getUserID(){
        ResultSet rs = null;
        int id = 0;
        String query = String.format("SELECT userid AS 'ID' FROM user WHERE name=%s", "'"+name+"'");
        try {
            rs = DBConnection.getData(query);
            while(rs.next()) {
                id = rs.getInt("ID");
            }
        } catch (SQLException e) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception e) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
