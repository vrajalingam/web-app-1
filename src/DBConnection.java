import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
    static final String JDBC_DRIVER="com.mysql.jdbc.driver";
    static final String DB_URL = "jdbc:mysql://localhost/servlet_db";

    static final String USER = "root";
    static final String PASS = "vihanga123";
    public static Connection con;

    public static Connection getCon() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to a selected database...");
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public static ResultSet getData(String sql) throws Exception {
        return getCon().createStatement().executeQuery(sql);
    }

    public static void setData(String sql) throws Exception {
        getCon().createStatement().executeUpdate(sql);
    }

    public static PreparedStatement prepareData(String sql) throws Exception {
        return getCon().prepareStatement(sql);
    }
}
