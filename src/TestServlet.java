import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class TestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader =request.getReader();
        Data data = new Gson().fromJson(reader, Data.class);
        data.insertUser();

        data.setId(data.getUserID());

        String data2 = new Gson().toJson(data, Data.class);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(data2);
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Hello");
    }
}
